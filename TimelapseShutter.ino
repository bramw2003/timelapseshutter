#define BLYNK_PRINT Serial


#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut icon).
char auth[] = "##################";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "";
char pass[] = "";

int BulbDelay =5;
int TimeLapseInterval = 5;
int TimeLapsePics = 24;

WidgetLED led1(V1);
WidgetLED led2(V2);

BlynkTimer timer;
BlynkTimer shutter;

BLYNK_WRITE(V2) // Trigger the shutter once
{
  Serial.println(param.asInt());
  if(param.asInt() == 1) {     // if Button sends 1
    Shutter();             // start the function
  }
}

BLYNK_WRITE(V3) // Trigger the shutter for as long as you hold down the button
{
  Serial.println(param.asInt());
  if(param.asInt() == 1) {     // if Button sends 1
    digitalWrite(23,LOW);            // start the function
  }
  else
  {
    digitalWrite(23,HIGH);
  }
}

BLYNK_WRITE(V4) // Set the BulbDelay 
{
  BulbDelay = 1000 + param.asInt() * 1000;// start the function
  Serial.println(BulbDelay);
  
  
  
}
BLYNK_WRITE(V5) // Bulb mode with the BulbDelay
{
  Serial.println(param.asInt());
  if(param.asInt() == 1) {     // if Button sends 1
    digitalWrite(23,LOW);            // start the function
    delay(BulbDelay);
    digitalWrite(23,HIGH);
  }
  else
  {
    digitalWrite(23,HIGH);
  }
}

BLYNK_WRITE(V6) // Set the TimeLapseIntverval 
{
  TimeLapseInterval = param.asInt()*1000;// start the function
  Serial.println(TimeLapseInterval);
}
BLYNK_WRITE(V7) // Set the TimeLapsePics 
{
  TimeLapsePics = param.asInt();// start the function
  Serial.println(TimeLapsePics);
}
BLYNK_WRITE(V8){
  if(param.asInt() == 1){
    for(int i = 0; i < TimeLapsePics; i++){
      Shutter();
      delay(TimeLapseInterval);
    }
  }
}

void Shutter(){
  digitalWrite(23,LOW);
    delay(300);
    digitalWrite(23,HIGH);
}
// V1 LED Widget is blinking
void blinkLedWidget()
{
  if (led1.getValue()) {
    led1.off();
    digitalWrite(23, HIGH);
    Serial.println("LED on V1: off");
  } else {
    led1.on();
    digitalWrite(23, LOW);
    Serial.println("LED on V1: on");
  }
}

void Shoot(){
  Serial.println(led2.getValue());
  if (led2.getValue()==1) {

    digitalWrite(23,LOW);
    delay(50);
    digitalWrite(23,HIGH);
    Serial.println("LED on V2: off");
        led2.off();
  } else {
    Serial.println("LED on V2: on");
  }
}
void setup()
{
  // Debug console
  Serial.begin(9600);
  pinMode(23,OUTPUT);
  digitalWrite(23,HIGH);
  Blynk.begin(auth, ssid, pass);
  // You can also specify server:
  //Blynk.begin(auth, ssid, pass, "blynk-cloud.com", 80);
  //Blynk.begin(auth, ssid, pass, IPAddress(192,168,1,100), 8080);

  //timer.setInterval(1000L, blinkLedWidget);
  //shutter.setInterval(100L, Shoot);
}

void loop()
{
  Blynk.run();
  //timer.run();
  //shutter.run();
}
